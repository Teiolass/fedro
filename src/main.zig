const std = @import("std");
const Allocator = std.mem.Allocator;

const Input = std.fs.File.Reader;
const Output = std.fs.File.Writer;

var logger: std.fs.File.Writer = undefined;
fn log(comptime fmt: []const u8, payload: anytype) void {
    logger.print(fmt, payload) catch return;
}

pub fn main() !void {
    const file = try std.fs.createFileAbsolute("/home/teiolass/tmp/fedro/test/log.txt", .{});
    defer file.close();
    logger = file.writer();
    log("Start logging...\n", .{});
    errdefer |err| log("Closing with error: {s}", .{@errorName(err)});

    const input = std.io.getStdIn().reader();
    const output = std.io.getStdOut().writer();

    var gp_allocator: std.heap.GeneralPurposeAllocator(.{ .thread_safe = false }) = .{};
    defer {
        const leaks = gp_allocator.deinit();
        if (leaks) log("WARNING: we are leaking\n", .{});
    }
    const allocator = gp_allocator.allocator();

    var server = Server.init(allocator);
    defer server.deinit();

    {
        var inner_arena = std.heap.ArenaAllocator.init(allocator);
        defer inner_arena.deinit();
        const inner_allocator = inner_arena.allocator();
        const init_settings = try handleInitialize(inner_allocator, input, output);
        try setupWorkspace(&server, init_settings.workspace);
        try handleInitialized(inner_allocator, input);
    }

    while (true) {
        const action = try handleGenericMessage(&server, input, output);
        switch (action) {
            .do_nothing => {},
            .quit => break,
        }
    }

    log("\nThat's All Folk\n", .{});
}

const FileId = usize;
const FilePosition = struct {
    file: FileId,
    cursor: CursorPosition,
};
const CursorPosition = struct {
    line: u16,
    character: u16,
};
const Request = struct {
    id: ?i64,
    content: Content,

    const Content = union(enum) {
        empty,
        goto: Goto,
        completion: Completion,
        file_set: FileSet,
    };
    const Goto = struct {
        postion: FilePosition,
    };
    const Completion = struct {
        postion: FilePosition,
    };
    const FileSet = struct {
        file: FileId,
        text: []const u8,
    };
};

const Server = struct {
    allocator: Allocator,
    file_mapper: std.StringHashMap(FileId),
    files_content: std.ArrayList(FileEntry),
    files_uri: std.ArrayList(Uri),
    first_empty_slot: ?FileId,

    const FileEntry = union(enum) {
        none: ?usize, // this takes too much space
        some: FileText,
    };
    const FileText = struct {
        text: []const u8,
        line_starts: std.ArrayListUnmanaged(u16),
    };

    const Self = @This();
    fn init(allocator: Allocator) Self {
        return .{
            .allocator = allocator,
            .files_content = std.ArrayList(FileEntry).init(allocator),
            .files_uri = std.ArrayList(Uri).init(allocator),
            .first_empty_slot = null,
            .file_mapper = std.StringHashMap(FileId).init(allocator),
        };
    }

    fn deinit(server: *Self) void {
        for (server.files_content.items) |*item, it_index| {
            if (item.* == .some) {
                server.allocator.free(item.some.text);
                item.some.line_starts.deinit(server.allocator);
                const uri_ptr = server.files_uri.items[it_index];
                server.allocator.free(uri_ptr.uri);
            }
        }
        server.files_content.deinit();
        server.files_uri.deinit();
        server.file_mapper.deinit();
    }
};

fn fulfillRequest(server: *Server, request: Request) !void {
    switch (request.content) {
        .empty, .goto, .completion => {},
        .file_set => return fulfillFileSet(server, request),
    }
}

fn fulfillFileSet(server: *Server, request: Request) !void {
    const content = request.content.file_set;
    const new_text = try server.allocator.alloc(u8, content.text.len);
    std.mem.copy(u8, new_text, content.text);
    const file = server.file.items[content.file];
    if (file == .some) {
        server.allocator.free(file.some);
    }
    file = .{ .some = new_text };
}

fn setupWorkspace(server: *Server, absolute_path: []const u8) !void {
    const accepted_extension = ".md";
    var uri_list = std.ArrayList(Uri).init(server.allocator);
    defer uri_list.deinit();
    try exploreDirectory(server.allocator, absolute_path, &uri_list);
    for (uri_list.items) |uri| {
        if (!std.mem.eql(u8, uri.uri[uri.extension..], accepted_extension)) continue;
        const slot = try getFreeFileIndex(server);
        server.files_uri.items[slot] = uri;
        try server.file_mapper.put(uri.getName(), slot);
    }
}

const Uri = struct {
    uri: []const u8,
    path: u8,
    name: u8,
    extension: u8,

    const Self = @This();

    pub fn fromString(string: []const u8) !Self {
        const err = error.bad_uri;
        var cursor: u8 = 0;
        while (cursor < string.len) : (cursor += 1) {
            const c = string[cursor];
            if (c == ':') break;
            if (!std.ascii.isAlphabetic(c)) return err;
        } else {
            return err;
        }
        if (string[cursor + 1] != '/') return err;
        if (string[cursor + 2] != '/') return err;
        cursor += 3;
        const path = cursor;
        var last_slash = cursor;
        while (cursor < string.len) : (cursor += 1) {
            const c = string[cursor];
            if (c == '/') last_slash = cursor;
        }
        const name = last_slash + 1;
        cursor = name;
        var extension: u8 = @intCast(u8, string.len);
        while (cursor < string.len) : (cursor += 1) {
            const c = string[cursor];
            if (c == '.') extension = cursor;
        }
        return .{
            .uri = string,
            .path = path,
            .name = name,
            .extension = extension,
        };
    }

    pub inline fn getName(uri: Self) []const u8 {
        return uri.uri[uri.name..uri.extension];
    }

    pub inline fn getPath(uri: Self) []const u8 {
        return uri.uri[uri.path..];
    }
};

fn exploreDirectory(allocator: Allocator, directory_path: []const u8, list: *std.ArrayList(Uri)) !void {
    const workspace_dir = try std.fs.openIterableDirAbsolute(directory_path, .{});
    var iter = workspace_dir.iterate();
    while (try iter.next()) |entry| {
        switch (entry.kind) {
            .File => {
                const name = entry.name;
                const scheme = "file://";
                const total_len = scheme.len + directory_path.len + 1 + name.len;
                const uri_string = blk: {
                    const uri_string = try allocator.alloc(u8, total_len);
                    std.mem.copy(u8, uri_string, scheme);
                    const abs_path_offset = scheme.len;
                    std.mem.copy(u8, uri_string[abs_path_offset..], directory_path);
                    uri_string[abs_path_offset + directory_path.len] = '/';
                    const name_offset = abs_path_offset + directory_path.len + 1;
                    std.mem.copy(u8, uri_string[name_offset..], name);
                    break :blk uri_string;
                };
                const uri = try Uri.fromString(uri_string);
                try list.append(uri);
            },
            .Directory => {
                const name = entry.name;
                var buffer: [1024]u8 = undefined;
                std.mem.copy(u8, &buffer, directory_path);
                buffer[directory_path.len] = '/';
                std.mem.copy(u8, buffer[directory_path.len + 1 ..], name);
                const total_length = directory_path.len + name.len + 1;
                try exploreDirectory(allocator, buffer[0..total_length], list);
            },
            else => {},
        }
    }
}

fn getFreeFileIndex(server: *Server) !FileId {
    if (server.first_empty_slot) |slot| {
        server.first_empty_slot = server.files_content.items[slot].none;
        return slot;
    }
    const slot = server.files_content.items.len;
    const new_content = try server.files_content.addOne();
    new_content.* = Server.FileEntry{ .none = null };
    _ = try server.files_uri.addOne();
    return slot;
}

const Method = enum {
    @"shutdown",
    @"textDocument/didOpen",
    @"textDocument/definition",
    @"textDocument/completion",
    @"textDocument/didChange",
    unrecognized, // Not in the specifications
};
const method_associator = blk: {
    const fields = @typeInfo(Method).Enum.fields;
    const Entry = struct {
        @"0": []const u8,
        @"1": Method,
    };
    var source: [fields.len]Entry = undefined;
    for (fields) |field, it_index| {
        source[it_index] = .{
            .@"0" = field.name,
            .@"1" = @intToEnum(Method, field.value),
        };
    }
    const ret = std.ComptimeStringMap(Method, source);
    break :blk ret;
};

const Message = struct {
    value: std.json.Value,
    id: ?i64,
};

const ConfigLogParsedJson = struct {
    indentation: u8 = 0,
};
fn logParsedJson(value: std.json.Value, config: ConfigLogParsedJson) !void {
    var i: u8 = 0;
    while (i < config.indentation) : (i += 1) {
        log("    ", .{});
    }
    switch (value) {
        .Object => {
            const object = value.Object;
            var iterator = object.iterator();
            log("Object = \n", .{});
            while (iterator.next()) |entry| {
                const el = entry.value_ptr.*;
                const name = entry.key_ptr.*;
                var j: u8 = 0;
                while (j < config.indentation) : (j += 1) {
                    log("    ", .{});
                }
                log("{s}:\n", .{name});
                try logParsedJson(el, .{ .indentation = config.indentation + 1 });
            }
        },
        .Array => {
            log("Array = \n", .{});
            for (value.Array.items) |entry| {
                try logParsedJson(entry, .{ .indentation = config.indentation + 1 });
            }
        },
        .String => log("String = \"{}\"\n", .{std.zig.fmtEscapes(value.String)}),
        .Integer => log("Integer = {d}\n", .{value.Integer}),
        .Bool => log("Bool = {}\n", .{value.Bool}),
        else => log("[UNR] {s}\n", .{@tagName(value)}),
    }
}

const InitSettings = struct {
    workspace: []const u8,
};
fn handleInitialize(allocator: Allocator, input: Input, output: Output) !InitSettings {
    var arena = std.heap.ArenaAllocator.init(allocator);
    defer arena.deinit();
    const inner_allocator = arena.allocator();

    const message = try nextMessage(allocator, input);

    var parser = std.json.Parser.init(allocator, false);
    const value_tree = try parser.parse(message);

    log("INITIALIZE\n", .{});
    try logParsedJson(value_tree.root, .{});
    log("\n\n", .{});

    const request_id = value_tree.root.Object.get("id").?.Integer;
    const params = value_tree.root.Object.get("params").?.Object;
    const method = value_tree.root.Object.get("method").?.String;
    const root_path = params.get("rootPath").?.String;
    std.debug.assert(std.mem.eql(u8, method, "initialize"));
    const response = .{ .capabilities = .{ .definitionProvider = true, .textDocumentSync = .{
        .openClose = true,
        .change = 1,
    }, .completionProvider = .{
        .triggerCharacters = [_][]const u8{"@"},
    } } };
    try send(response, request_id, inner_allocator, output);

    return .{
        .workspace = root_path,
    };
}

fn handleInitialized(allocator: Allocator, input: Input) !void {
    var arena = std.heap.ArenaAllocator.init(allocator);
    defer arena.deinit();
    const inner_allocator = arena.allocator();

    const message = try nextMessage(inner_allocator, input);
    var parser = std.json.Parser.init(allocator, false);
    const value_tree = try parser.parse(message);

    log("INITIALIZED\n", .{});
    try logParsedJson(value_tree.root, .{});
    log("\n\n", .{});

    const method = value_tree.root.Object.get("method").?.String;
    std.debug.assert(std.mem.eql(u8, method, "initialized"));
}

const Action = enum {
    do_nothing,
    quit,
};
fn handleGenericMessage(server: *Server, input: Input, output: Output) !Action {
    var arena = std.heap.ArenaAllocator.init(server.allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const message = try nextMessage(allocator, input);
    var parser = std.json.Parser.init(allocator, false);
    const value_tree = try parser.parse(message);
    const method_name = value_tree.root.Object.get("method").?.String;
    const request_id = if (value_tree.root.Object.get("id")) |x| x.Integer else null; // when null it's a notification
    const method = if (method_associator.get(method_name)) |m| m else Method.unrecognized;

    log("NEW MESSAGE with method `{s}`:\n", .{@tagName(method)});
    try logParsedJson(value_tree.root, .{});
    log("\n\n", .{});

    const value = blk: { // if root hasnt params, we default to an empty set
        if (value_tree.root.Object.get("params")) |value| {
            break :blk value;
        } else {
            const empty_map = std.json.ObjectMap.init(allocator);
            const value = std.json.Value{ .Object = empty_map };
            break :blk value;
        }
    };
    const message_value: Message = .{
        .value = value,
        .id = request_id,
    };
    switch (method) {
        .@"shutdown" => return .quit,
        .@"textDocument/definition" => try handleDefinition(server, message_value, output),
        .@"textDocument/completion" => try handleCompletion(server, message_value, output),
        .@"textDocument/didOpen" => try handleDidOpen(server, message_value),
        .@"textDocument/didChange" => try handleDidChange(server, message_value),
        .unrecognized => {},
    }
    return .do_nothing;
}

fn send(payload: anytype, id: ?i64, allocator: Allocator, stream: Output) !void {
    const Payload = struct {
        id: ?i64,
        jsonrpc: []const u8 = "2.0",
        result: @TypeOf(payload),
    };

    const message = Payload{
        .id = id,
        .result = payload,
    };
    var arena = std.heap.ArenaAllocator.init(allocator);
    defer arena.deinit();
    var buffer = std.ArrayList(u8).init(arena.allocator());
    var writer = buffer.writer();
    try std.json.stringify(message, .{}, writer);

    log("SENDING MESSAGE:\n{s}\n", .{buffer.items});

    const len = buffer.items.len;
    try stream.print("Content-Length: {d}\r\n\r\n", .{len});
    try stream.print("{s}", .{buffer.items});

    log("Message sent\n\n", .{});
}

fn nextMessage(allocator: Allocator, input: Input) ![]u8 {
    const length = try getLength(input);
    std.debug.print("Lenght received", .{});
    try input.skipBytes(3, .{ .buf_size = 3 });
    const buffer = try allocator.alloc(u8, length);
    _ = try input.read(buffer);
    return buffer;
}

fn getLength(input: Input) !usize {
    const util = struct {
        fn isNumber(c: u8) bool {
            return c >= '0' and c <= '9';
        }
    };
    var buffer: [128]u8 = undefined;
    const slice = try input.readUntilDelimiter(&buffer, '\r');

    const number_position = blk: {
        var start: usize = undefined;
        var end: usize = undefined;
        var is_started = false;
        for (slice) |c, it_index| {
            if (!is_started and util.isNumber(c)) {
                is_started = true;
                start = it_index;
            } else if (is_started and !util.isNumber(c)) {
                end = it_index;
                break :blk .{
                    .start = start,
                    .end = end,
                };
            }
        }
        break :blk .{
            .start = start,
            .end = slice.len,
        };
    };
    const number_slice = buffer[number_position.start..number_position.end];
    const length = try std.fmt.parseInt(usize, number_slice, 10);
    return length;
}

fn handlePosition(server: *Server, object: std.json.ObjectMap) !FilePosition {
    const doc = object.get("textDocument").?;
    const uri = doc.Object.get("uri").?.String;
    const uri_begin = "file://";
    std.debug.assert(std.mem.eql(u8, uri_begin, uri[0..uri_begin.len]));
    const path = uri[uri_begin.len..];
    const filename = justFilename(path);
    const file_id = if (server.file_mapper.get(filename)) |id| id else {
        log("ERROR: file not found (`{s}`)", .{filename});
        return error.unknown_file;
    };
    const position = object.get("position").?.Object;
    const line = position.get("line").?.Integer;
    const character = position.get("character").?.Integer;
    return .{
        .file = file_id,
        .cursor = .{
            .line = @intCast(u16, line - 1),
            .character = @intCast(u16, character),
        },
    };
}

fn handleDefinition(server: *Server, message: Message, output: Output) !void {
    var arena = std.heap.ArenaAllocator.init(server.allocator);
    defer arena.deinit();
    const inner_allocator = arena.allocator();

    const start_position = handlePosition(server, message.value.Object) catch |err| {
        if (err == error.unknown_file) return;
        return err;
    };

    const file_text = server.files_content.items[start_position.file].some;
    const link = findLinkAt(file_text, start_position.cursor) orelse return;
    const link_key = justFilename(link);
    const link_id = server.file_mapper.get(link_key) orelse return;
    const uri = server.files_uri.items[link_id].uri;

    const to_send = .{ .uri = uri, .range = .{
        .start = .{
            .line = 0,
            .character = 0,
        },
        .end = .{
            .line = 0,
            .character = 0,
        },
    } };
    try send(to_send, message.id, inner_allocator, output);
}

fn handleCompletion(server: *Server, message: Message, output: Output) !void {
    var arena = std.heap.ArenaAllocator.init(server.allocator);
    defer arena.deinit();
    const inner_allocator = arena.allocator();

    const position = message.value.Object.get("position").?.Object;
    const line = position.get("line").?.Integer;
    const character = position.get("character").?.Integer;
    const uri = message.value.Object.get("textDocument").?.Object.get("uri").?.String;
    _ = line;
    _ = character;
    _ = uri;

    const Label = struct { label: []const u8 };
    var list = std.ArrayList(Label).init(inner_allocator);
    var iterator = server.file_mapper.keyIterator();
    while (iterator.next()) |key| {
        try list.append(.{ .label = key.* });
    }
    const response = .{ .isIncomplete = false, .items = list.items };
    try send(response, message.id, inner_allocator, output);
}

fn handleDidOpen(server: *Server, message: Message) !void {
    const doc = message.value.Object.get("textDocument").?;
    const raw_text = doc.Object.get("text").?.String;
    const uri = doc.Object.get("uri").?.String;
    const file_name = justFilename(uri);
    const file_id = server.file_mapper.get(file_name) orelse return;

    const text = try server.allocator.dupe(u8, raw_text);
    var content = &server.files_content.items[file_id];
    try setText(server, content, text);
}

fn handleDidChange(server: *Server, message: Message) !void {
    const doc = message.value.Object.get("textDocument").?;
    const uri = doc.Object.get("uri").?.String;
    const file_name = justFilename(uri);
    const file_id = server.file_mapper.get(file_name) orelse return;
    const changes = message.value.Object.get("contentChanges").?;
    const change = changes.Array.items[0].Object;
    const raw_text = change.get("text").?.String;

    const text = try server.allocator.dupe(u8, raw_text);
    var content = &server.files_content.items[file_id];
    try setText(server, content, text);
}

fn setText(server: *Server, content: *Server.FileEntry, text: []const u8) !void {
    if (content.* == .some) {
        server.allocator.free(content.some.text);
        content.some.line_starts.deinit(server.allocator);
    }
    var new_line_starts = std.ArrayList(u16).init(server.allocator);
    try appendLineStart(text, &new_line_starts);
    new_line_starts.shrinkRetainingCapacity(new_line_starts.items.len);
    const file_text = .{ .text = text, .line_starts = new_line_starts.moveToUnmanaged() };
    content.* = .{ .some = file_text };
}

fn justFilename(path: []const u8) []const u8 {
    if (path.len == 0) return path;
    var cursor = path.len - 1;
    const end = blk: while (path[cursor] != '.') {
        if (cursor <= 0) {
            break :blk path.len;
        }
        cursor -= 1;
    } else {
        break :blk cursor;
    };
    while (path[cursor] != '/') {
        if (cursor <= 0) return path[0..end];
        cursor -= 1;
    }
    const beg = cursor + 1;
    return path[beg..end];
}

fn findLinkAt(content: Server.FileText, position: CursorPosition) ?[]const u8 {
    const starting_cursor = content.line_starts.items[position.line] + position.character + 1;
    const end = blk: {
        var cursor = starting_cursor;
        while (cursor < content.text.len) {
            if (content.text[cursor] == ')') break :blk cursor;
            if (content.text[cursor] == '\n') return null;
            cursor += 1;
        } else return null;
    };
    const start = blk: {
        var cursor = starting_cursor;
        while (true) {
            if (content.text[cursor] == '(') break :blk cursor + 1;
            if (content.text[cursor] == '\n') return null;
            if (cursor == 0) return null;
            cursor -= 1;
        }
    };
    const match = "@file";
    if (start > match.len and std.mem.eql(u8, match, content.text[start - match.len - 1 .. start - 1]))
        return content.text[start..end];
    return null;
}

/// Doesnt handle higher than file length cursors!!
fn getPositionAt(cursor: usize, file: Server.FileText) CursorPosition {
    const data = file.line_starts.items;
    var sx = 0;
    var dx = data.len;
    const target = @intCast(u16, cursor);

    while (sx < dx) {
        const mid = (sx + dx) / 2;
        if (target > data[mid]) sx = mid else dx = mid;
    }
    const line = sx;
    const character = cursor - data[line];
    return .{ .line = line, .character = character };
}

/// appends indexes at which the text is exactly '\n'
fn appendLineStart(text: []const u8, vector: *std.ArrayList(u16)) !void {
    for (text) |it, it_index| {
        if (it == '\n') try vector.append(@intCast(u16, it_index));
    }
}
